/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

namespace vesuvius::core::math {

template <typename T, unsigned int N>
struct vec {
	static_assert(N <= 4, "Invalid vector size!");
};

template <typename T>
struct vec<T, 1> {
	union { T x;  T i; T r; };
};

template <typename T>
struct vec<T, 2> : public vec<T, 1> {
	union { T y;  T j; T b; };
};

template <typename T>
struct vec<T, 3> : public vec<T, 2> {
	union { T z;  float k; T g; };
};

template <typename T>
struct vec<T, 4> : public vec<T, 3> {
	union { T w;  T l; T a; };
};

} // namespace vesuvius::core::math
