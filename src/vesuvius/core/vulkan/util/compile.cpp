/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "vesuvius/core/vulkan/util/compile.hpp"

#include <iostream>

#include <boost/process.hpp>

#include <vesuvius/core/vulkan/exceptions.hpp>
#include <vesuvius/core/io/exceptions.hpp>

namespace vesuvius::core::vulkan {

void CompileShader(std::filesystem::path exe, std::filesystem::path inputFile, std::filesystem::path outputFile) {
	boost::process::ipstream pipe_stream, err_stream;

	//system((exe.string() + " -V " + inputFile.string() + " -o " + outputFile.string()).c_str());
	boost::process::child c(exe.string() + " -V " + inputFile.string() + " -o " + outputFile.string(),
		boost::process::std_out > pipe_stream, boost::process::std_err > err_stream);


	std::string line;
	while (pipe_stream && std::getline(pipe_stream, line) && !line.empty()) {
		std::cout << line << std::endl;
	}
	std::stringstream cerr;
	while (err_stream && std::getline(err_stream, line) && !line.empty()) {
		std::cout << line << std::endl;
		cerr << line << std::endl;
	}
	c.wait();

	c.join();
	if (int err = c.exit_code(); err != 0) {
		BOOST_THROW_EXCEPTION(Exception{}
			<< MessageInfo("Shader compilation failure")
			<< ValidatorErrnoInfo(err)
			<< CerrInfo(cerr.str())
		);
	}
}

std::vector<char> ReadFile(const std::filesystem::path& fileName) {
	std::ifstream file(fileName, std::ios::in | std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		BOOST_THROW_EXCEPTION(io::Exception{} 
			<< io::MessageInfo("Unable to open file") 
			<< io::FileInfo(fileName)
		);
	}
	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}


} // namespace vesuvius::core::vulkan::shader