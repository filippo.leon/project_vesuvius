/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <boost/throw_exception.hpp>
#include <boost/exception/exception.hpp>
#include <boost/exception/info.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception/errinfo_at_line.hpp>
#include <boost/exception/errinfo_file_name.hpp>
#include <boost/exception/errinfo_api_function.hpp>

namespace vesuvius::core::vulkan {

using ValidatorErrnoInfo = boost::error_info<struct ValidatorErrnoTag, int>;
using ResultErrnoInfo = boost::error_info<struct ResultErrnoTag, VkResult>;
using MessageInfo = boost::error_info<struct MessageTag, std::string>;
using CerrInfo = boost::error_info<struct CerrTag, std::string>;

class Exception : public virtual boost::exception, public virtual std::exception {
	virtual const char* what() const noexcept {
		return boost::diagnostic_information_what(*this);
	}
};

inline void CheckResult(const VkResult &res) {
	if (res != VK_SUCCESS) {
		BOOST_THROW_EXCEPTION(Exception{} << ResultErrnoInfo(res));
	}
}

} // namepsace vesuvius::core::vulkan
