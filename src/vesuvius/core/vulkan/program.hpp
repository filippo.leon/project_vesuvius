/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <variant>
#include <string>
#include <optional>
#include <unordered_map>

typedef struct VkShaderModule_T *VkShaderModule;

namespace vesuvius::core::vulkan {

class LogicalDevice;

using PreprocessorValue = std::variant<int, std::string, bool>;

struct ProgramConfig {
	std::unordered_map<std::string, PreprocessorValue> defines;
	std::string vertexShader;
	std::string fragmentShader;
	std::optional<std::string> geometryShader;
};

class Program {
public:
	Program(const LogicalDevice &device, std::string name, const ProgramConfig &config);

	~Program();
private:
	VkShaderModule m_fragmentModule, m_vertexModule;

	const LogicalDevice &m_device;
};

} // namespace vesuvius::core::vulkan