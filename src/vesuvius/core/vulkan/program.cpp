/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "vesuvius/core/vulkan/program.hpp"

#include <filesystem>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "vesuvius/core/app/resources.hpp"
#include "vesuvius/core/vulkan/util/compile.hpp"
#include "vesuvius/core/vulkan/exceptions.hpp"
#include "vesuvius/core/vulkan/device.hpp"

namespace vesuvius::core::vulkan {

namespace {

VkShaderModule CompileAndLoadShader(const LogicalDevice &device, const std::filesystem::path &exe, const std::string &name) {
	std::filesystem::path spvFile = app::GetAppDataPath("shadersBin/" + name + ".frag.spv");
	core::vulkan::CompileShader(exe,
		app::GetDataPath("shaders/" + name + ".frag"), spvFile);

	std::vector<char> data = ReadFile(spvFile);

	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = data.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(data.data());

	VkShaderModule shaderModule;
	CheckResult(vkCreateShaderModule(device.Get(), &createInfo, nullptr, &shaderModule));

	return shaderModule;
}

} // namespace

Program::Program(const LogicalDevice &device, std::string name, const ProgramConfig &config) : m_device(device) {
	std::filesystem::path exe("..\\bin\\glslangValidator.exe");

	m_fragmentModule = CompileAndLoadShader(m_device, exe, config.fragmentShader);
	m_vertexModule = CompileAndLoadShader(m_device, exe, config.vertexShader);
}

Program::~Program() {
	vkDestroyShaderModule(m_device.Get(), m_fragmentModule, nullptr);
	vkDestroyShaderModule(m_device.Get(), m_vertexModule, nullptr);
}

} // namespace vesuvius::core::vulkan