﻿cmake_minimum_required (VERSION 3.8)

file(GLOB_RECURSE vesuvius_SRC "**/*.cpp")
file(GLOB_RECURSE vesuvius_HDR "**/*.hpp")

file(TO_CMAKE_PATH "${CMAKE_SOURCE_DIR}/data/" sourceDirData)
file(TO_CMAKE_PATH "${CMAKE_BINARY_DIR}/data/" binaryDirData)
add_definitions(-DVESUVIUS_DATA_PATH="${sourceDirData}/")
add_definitions(-DVESUVIUS_APPDATA_PATH="${binaryDirData}/")

add_library(vesuvius ${vesuvius_SRC} ${vesuvius_HDR})
target_link_libraries(vesuvius PUBLIC Vulkan::Vulkan fmt::fmt Boost::boost Boost::filesystem Boost::system Threads::Threads glfw)
target_link_libraries(vesuvius PUBLIC ${LUA_LIBRARY})
target_include_directories(vesuvius PUBLIC ${LUA_INCLUDE_DIR})
target_include_directories(vesuvius PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${vesuvius_HDR})

if (MSVC)
	 target_compile_options(vesuvius 
		PUBLIC /permissive- 
		#PUBLIC /Za # Not needed with permissive- (breaks boost::process)
	)
elseif(GNU or CLANG)
	target_compile_options(vesuvius 
		PUBLIC -Wall
		PUBLIC -Wextra
		PUBLIC -pedantic
	)
endif()