#include <optional>
#include <vector>

#include <iostream>
#include <fstream>
#include <set>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vesuvius/core/glfw/exceptions.hpp>
#include <vesuvius/core/vulkan/exceptions.hpp>
#include <vesuvius/core/vulkan/program.hpp>
#include <vesuvius/core/vulkan/device.hpp>
#include <vesuvius/core/math/vec.hpp>
#include <vesuvius/core/math/math.hpp>

using namespace vesuvius;

static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
	if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
		return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}

	return availableFormats[0];
}

VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for (const auto& availablePresentMode : availablePresentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		}
		else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			bestMode = availablePresentMode;
		}
	}

	return bestMode;
}

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, const core::math::vec<int, 2> &size) {
	VkExtent2D actualExtent = { (uint32_t)size.x, (uint32_t)size.y };

	actualExtent.width = core::math::Clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
	actualExtent.height = core::math::Clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

	return actualExtent;
}

int main() {
	struct Config {
		std::string appName = "My App";
		std::string windowName = appName;
		core::math::vec<int, 2> resolution{ 600, 700 };
	} config;

	glfwSetErrorCallback([](int err, const char *msg) {
		BOOST_THROW_EXCEPTION(core::glfw::Exception{} 
			<< core::glfw::ErrnoInfo(err)
			<< core::glfw::MessageInfo(msg));
	});
	glfwInit();
	if (!glfwVulkanSupported()) {
		BOOST_THROW_EXCEPTION(core::glfw::Exception{} << core::glfw::MessageInfo("GLFW: Vulkan not supported"));
	}

	uint32_t count;
	const char **extensions = glfwGetRequiredInstanceExtensions(&count);

	VkApplicationInfo appInfo{};
	appInfo.pApplicationName = config.appName.c_str();

	VkInstanceCreateInfo instanceCreateInfo{};
	instanceCreateInfo.pApplicationInfo = &appInfo;
	instanceCreateInfo.enabledExtensionCount = count;
	instanceCreateInfo.ppEnabledExtensionNames = extensions;

	VkInstance instance{};
	core::vulkan::CheckResult(vkCreateInstance(&instanceCreateInfo, nullptr, &instance));

	uint32_t ncount;
	core::vulkan::CheckResult(vkEnumeratePhysicalDevices(instance, &ncount, nullptr));
	std::vector<VkPhysicalDevice> physicalDevices(ncount);
	core::vulkan::CheckResult(vkEnumeratePhysicalDevices(instance, &ncount, physicalDevices.data()));

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow *window = glfwCreateWindow(config.resolution.x, config.resolution.y, config.windowName.c_str(), nullptr, nullptr);
	if (!window) {
		BOOST_THROW_EXCEPTION(core::glfw::Exception{} << core::glfw::MessageInfo("Couldn't open window"));
	}

	VkSurfaceKHR surface;
	core::vulkan::CheckResult(glfwCreateWindowSurface(instance, window, nullptr, &surface));

	const std::vector<const char *> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	struct DeviceCapabilities {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	std::optional<VkPhysicalDevice> selectedDevice;
	std::optional<DeviceCapabilities> selectedDeviceProperties;
	for (VkPhysicalDevice device : physicalDevices) {
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);

		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

		VkSurfaceCapabilitiesKHR capabilities;
		core::vulkan::CheckResult(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &capabilities));

		std::vector<VkSurfaceFormatKHR> formats;
		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
		if (formatCount != 0) {
			formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, formats.data());
		}

		std::vector<VkPresentModeKHR> presentModes;
		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
		if (presentModeCount != 0) {
			presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, presentModes.data());
		}

		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
			&& deviceFeatures.geometryShader
			&& deviceFeatures.tessellationShader 
			&& requiredExtensions.empty()
			&& presentModeCount > 0 && formatCount > 0) {
			selectedDevice = device;
			selectedDeviceProperties = {
				capabilities, formats, presentModes
			};
		}
	}

	if (!selectedDevice) {
		BOOST_THROW_EXCEPTION(core::vulkan::Exception{} << core::vulkan::MessageInfo("No suitable device found"));
	}

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(*selectedDevice, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(*selectedDevice, &queueFamilyCount, queueFamilies.data());

	uint32_t presentQueueFamilyIndex = 0;
	uint32_t graphicsQueueFamilyIndex = 0;
	uint32_t familyIndex = 0;
	for (const auto& queueFamily : queueFamilies) {
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			graphicsQueueFamilyIndex = familyIndex;
		}
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(*selectedDevice, familyIndex, surface, &presentSupport);
		if (queueFamily.queueCount > 0 && presentSupport) {
			presentQueueFamilyIndex = familyIndex;
		}
	}


	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = { graphicsQueueFamilyIndex, presentQueueFamilyIndex };

	float queuePriority = 1.0f;
	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO; 
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();
	createInfo.enabledLayerCount = 0;
	VkDevice device;
	core::vulkan::CheckResult(vkCreateDevice(*selectedDevice, &createInfo, nullptr, &device));

	core::vulkan::LogicalDevice logicalDevice(device);

	if (!glfwGetPhysicalDevicePresentationSupport(instance, *selectedDevice, presentQueueFamilyIndex)) {
		BOOST_THROW_EXCEPTION(core::glfw::Exception{}
		<< core::glfw::MessageInfo("Selected device/queue does not support presentation"));
	}

	VkQueue graphicsQueue;
	vkGetDeviceQueue(device, graphicsQueueFamilyIndex, 0, &graphicsQueue);

	VkQueue presentQueue;
	vkGetDeviceQueue(device, presentQueueFamilyIndex, 0, &presentQueue);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(selectedDeviceProperties->formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(selectedDeviceProperties->presentModes);
	VkExtent2D extent = chooseSwapExtent(selectedDeviceProperties->capabilities, config.resolution);

	uint32_t imageCount = selectedDeviceProperties->capabilities.minImageCount + 1;
	if (selectedDeviceProperties->capabilities.maxImageCount > 0 && imageCount > selectedDeviceProperties->capabilities.maxImageCount) {
		imageCount = selectedDeviceProperties->capabilities.maxImageCount;
	}

	uint32_t queueFamilyIndices[] = { graphicsQueueFamilyIndex, presentQueueFamilyIndex };
	VkSwapchainCreateInfoKHR swapchainCreateInfo{};
	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.minImageCount = imageCount;
	swapchainCreateInfo.imageFormat = surfaceFormat.format;
	swapchainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
	swapchainCreateInfo.imageExtent = extent;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (graphicsQueueFamilyIndex != presentQueueFamilyIndex) {
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapchainCreateInfo.queueFamilyIndexCount = 2;
		swapchainCreateInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}
	swapchainCreateInfo.preTransform = selectedDeviceProperties->capabilities.currentTransform;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.presentMode = presentMode;
	swapchainCreateInfo.clipped = VK_TRUE;
	swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	VkSwapchainKHR swapChain;
	core::vulkan::CheckResult(vkCreateSwapchainKHR(device, &swapchainCreateInfo, nullptr, &swapChain));

	std::vector<VkImage> swapChainImages;
	core::vulkan::CheckResult(vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr));
	swapChainImages.resize(imageCount);
	core::vulkan::CheckResult(vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data()));

	std::vector<VkImageView> swapChainImageViews;
	swapChainImageViews.resize(swapChainImages.size());
	for (size_t i = 0; i < swapChainImages.size(); i++) {
		VkImageViewCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = swapChainImages[i];
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = surfaceFormat.format;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY; createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		core::vulkan::CheckResult(vkCreateImageView(device, &createInfo, nullptr, &swapChainImageViews[i]));
	}

	glfwSetKeyCallback(window, KeyCallback);


	core::vulkan::ProgramConfig progConfig;
	progConfig.fragmentShader = "default";
	progConfig.vertexShader = "default";
	core::vulkan::Program prog(logicalDevice, "default", progConfig);

	while (!glfwWindowShouldClose(window)) {


		glfwPollEvents();
	}

	//vkDestroyImageView
	//destroy queues
	// destroy instance
	// destroy swapchain
}