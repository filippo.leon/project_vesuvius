#version  450
#extension GL_ARB_separate_shader_objects : enable

layout(location=1) in vec3 color;

layout(location=0) out vec4 fragColor;

void main() {
	fragColor = vec4(0.4,0.4,0.8,1.0);
}