#version 450

in vec3 vertexPos;

void main() {
	gl_Position = vertexPos;
}